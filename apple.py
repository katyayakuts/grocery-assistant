"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
from termcolor import colored


def main():
    """Add "яблоко" word."""
    please = "Пожалуйста,"
    n = int(input("Сколько яблок Вам нужно?\n"))
    if ((n < 21) and (n > 4)) or ((n > 24) and (n < 31)):
        print(please, n, "яблок", )
    elif (n == 1) or (n == 21):
        print(please, n, "яблоко")
    elif ((n < 5) and (n > 1)) or ((n > 21) and (n < 25)):
        print(please, n, "яблока")
    else:
        print("Столько нет")


def termcolor(text):
    welcome = colored(text, "red", attrs=["reverse", "blink"])
    print(welcome)


if __name__ == "__main__":
    # welcome = colored("Grocery assistant", "red", attrs=["reverse", "blink"])
    # print(welcome)
    #   cprint("Grocery assistant")  # color this caption
    text_welcome = "Grocery assistant"
    termcolor(text_welcome)
    main()
